from src.helpers import is_adult


def test_is_not_adult_for_age_17():
    assert not is_adult(17)


def test_is_adult_for_age_18():
    assert is_adult(18)


def test_is_adult_for_age_19():
    assert is_adult(19)
