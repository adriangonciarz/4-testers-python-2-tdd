from src.airplane import Airplane


def test_new_plane_milage_and_seats():
    plane1 = Airplane("Boeing 777", 550)
    assert plane1.mileage == 0
    assert plane1.seats_taken == 0


def test_plane_mileage():
    plane1 = Airplane("Boeing 777", 550)
    plane1.fly(5000)
    assert plane1.mileage == 5000


def test_plane_mileage_in_two_travels():
    plane1 = Airplane("Boeing 777", 550)
    plane1.fly(5000)
    plane1.fly(3000)
    assert plane1.mileage == 8000


def test_service_is_not_required():
    plane1 = Airplane("Boeing 777", 550)
    plane1.fly(9999)
    assert not plane1.is_service_required()


def test_service_is_required():
    plane1 = Airplane("Boeing 777", 550)
    plane1.fly(10001)
    assert plane1.is_service_required()


def test_available_seats():
    plane1 = Airplane("Boeing", 200)
    plane1.board_passengers(180)
    assert plane1.get_available_seats() == 20
    assert plane1.seats_taken == 180


def test_overboarded_airplane():
    plane1 = Airplane("Boeing", 200)
    plane1.board_passengers(201)
    assert plane1.get_available_seats() == 0
    assert plane1.seats_taken == 200
