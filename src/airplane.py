class Airplane:
    def __init__(self, name, number_of_seats):
        self.name = name
        self.mileage = 0
        self.number_of_seats = number_of_seats
        self.seats_taken = 0

    def fly(self, distance):
        self.mileage += distance

    def is_service_required(self):
        return self.mileage > 10000

    def board_passengers(self, number_of_passengers):
        if number_of_passengers + self.seats_taken <= self.number_of_seats:
            self.seats_taken += number_of_passengers
        else:
            self.seats_taken = self.number_of_seats

    def get_available_seats(self):
        return self.number_of_seats - self.seats_taken


if __name__ == '__main__':
    plane1 = Airplane("Boeing 777", 550)
    plane2 = Airplane("Airbus A380", 853)
    plane1.fly(10000)
    plane2.fly(10001)

    print(plane1.mileage)
    print(plane2.mileage)
    print(plane1.is_service_required())
    print(plane2.is_service_required())
    plane1.board_passengers(300)
    plane1.board_passengers(200)
    print(plane1.seats_taken)
    print(plane1.get_available_seats())
